package com.example.motionwidgetdatacollector;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class CollectedDataDB {
	private static Context ourContext;
	public static GyroDBHelper dbinstance;
	private SQLiteDatabase gyrodatabase;

	
	
	public static final String KEY_ROWID = "_id";
	public static final String ycoord = "y-coordinate";
	public static final String xcoord = "x-coordinate";
	public static final String zcoord = "z-coordinate";

	private static final String Gyro_DB = "Gyro_Data";
	private static final String DB_Table = "Gyro_Values";
	private static final int DB_Version = 1;
	
	public CollectedDataDB() {
		
	}
	
	private static class GyroDBHelper extends SQLiteOpenHelper{

		public GyroDBHelper(Context context) {
			super(context, Gyro_DB, null, DB_Version);
			
		}


		@Override
		public void onCreate(SQLiteDatabase dbase) {
			dbase.execSQL("CREATE TABLE" + DB_Table + " (" +
					KEY_ROWID + "Integer PRIMARY KEY AUTO INCREMENT, " +
					ycoord + "INTEGER" + xcoord + "INTEGER" + zcoord + "INTEGER);"
					
					);
					
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase dbase, int oldVersion, int newVersion) {
			dbase.execSQL("DROP TABLE IF EXISTS" + Gyro_DB);
			onCreate(dbase);
		}
	}


public CollectedDataDB(Context c){
	ourContext = c;
}

public CollectedDataDB open(){
	dbinstance = new GyroDBHelper(ourContext);
	gyrodatabase = dbinstance.getWritableDatabase();
	return this;

}

public void closeGyroDB(){
	dbinstance.close();
}

		
}


