package com.example.motionwidgetdatacollector;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.view.Menu;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MotionAdminActivity extends Activity {
	Button startDataCollection;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motion_admin);
        startDataCollection.setOnClickListener((OnClickListener) this);
      
        //Set up Sensor Manager within onClick Method
        
        SensorManager gyromanager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        if (gyromanager.getSensorList(Sensor.TYPE_GYROSCOPE).size() != 0) {
        	Sensor gyroscope = gyromanager.getSensorList(Sensor.TYPE_GYROSCOPE).get(0);
        	gyromanager.registerListener(this, gyroscope, SensorManager.SENSOR_DELAY_NORMAL);
        }
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_motion_admin, menu);
        return true;
    }
}
